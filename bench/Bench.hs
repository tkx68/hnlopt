{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE Strict #-}

module Main (main) where

import Gauge.Main
import Math.NLOpt
import Math.NLOpt.Examples

bSph :: Int -> Benchmark
bSph nDim =
  bgroup
    ("sph with nDim=" ++ show nDim)
    [ bench "fEps=1e-4" $ whnf crsMinimise $ sph 1000000 nDim 1e-4,
      bench "fEps=1e-6" $ whnf crsMinimise $ sph 1000000 nDim 1e-6
    ]

main :: IO ()
main =
  defaultMain
    [ bgroup
        "CRS"
        [ bSph 10,
          bSph 20,
          bSph 40,
          bgroup
            "ack"
            [ bench "fEps=1e-4" $ whnf crsMinimise $ ack 100000 1e-4,
              bench "fEps=1e-6" $ whnf crsMinimise $ ack 100000 1e-6,
              bench "fEps=1e-8" $ whnf crsMinimise $ ack 100000 1e-8
            ]
        ]
    ]
