{-# LANGUAGE Strict #-}

module Math.NLOpt
  ( -- * Definition of an optimisation problem
    module Math.NLOpt.Problem,

    -- * CRS optimisation
    module Math.NLOpt.CRS,
  )
where

import Math.NLOpt.CRS
import Math.NLOpt.Problem
