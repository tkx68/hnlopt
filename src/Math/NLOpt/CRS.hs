{-# LANGUAGE Strict #-}

module Math.NLOpt.CRS (crsMinimise) where

import Control.Monad.ST
import Data.Vector as V hiding ((++))
import Data.Vector.Unboxed as VU hiding ((++))
import Math.NLOpt.Point
import Math.NLOpt.Problem
import System.Random.MWC
import System.Random.Stateful

generateTrialPoint ::
  forall m g.
  (StatefulGen g m) =>
  Int ->
  (Point -> Double) ->
  Point ->
  Point ->
  g ->
  V.Vector (Point, Double) ->
  (Int, Int) ->
  m Point'
generateTrialPoint nDim f lb ub g s (iBest, iWorst) = genBetterPoint
  where
    fWorst = snd (s V.! iWorst)
    n = V.length s
    genBetterPoint :: m (Point, Double)
    {-# INLINE genBetterPoint #-}
    genBetterPoint = do
      x <- genBounded
      let fx = f x :: Double
      if fx >= fWorst
        then do
          x2 <- genWithLocalMutation (fst $ s V.! iBest) x
          let fx2 = f x2 :: Double
          if fx >= fWorst
            then genBetterPoint
            else pure (x2, fx2)
        else pure (x, fx)
    genWithLocalMutation :: Point -> Point -> m Point
    {-# INLINE genWithLocalMutation #-}
    genWithLocalMutation (Point xBest) (Point xNew) = do
      omega <- VU.replicateM nDim $ do uniformRM (0, 1) g
      pure . Point $ VU.zipWith3 (\w xb xn -> (1 + w) * xb - w * xn) omega xBest xNew
    genBounded :: m Point
    {-# INLINE genBounded #-}
    genBounded = do
      x <- genFromGlobalSimplex
      if bounded x (lb, ub)
        then pure x
        else genBounded
    genFromGlobalSimplex :: m Point
    {-# INLINE genFromGlobalSimplex #-}
    genFromGlobalSimplex = do
      is' <- V.replicateM (nDim - 1) $ uniformRM (0, n - 1) g -- n-1 randomly chosen (with replacement) indices excl. x_{n+1}
      let is = V.cons iBest is' -- plus the best one
      ilast <- uniformRM (0, n - 1) g
      let g0 = 2 / (fromIntegral nDim) :: Double
          selectPoint i = fst (s V.! i)
          selectedPoints = selectPoint <$> is
          twoG = scalePoint (addAllPoints selectedPoints) g0 -- centroid * 2
          x = subtractPoints twoG (fst $ s V.! ilast)
      pure x

crsTrial ::
  forall m g.
  (StatefulGen g m) =>
  Int ->
  (Point -> Double) ->
  (Point, Point) ->
  g ->
  SearchSample ->
  (Int, Int) ->
  m SearchSample
crsTrial nDim f (lb, ub) g s0 (ib, iw) = do
  pBetter <- generateTrialPoint nDim f lb ub g s0 (ib, iw)
  pure $ V.unsafeUpd s0 [(iw, pBetter)]

crsInit ::
  forall m g.
  (StatefulGen g m) =>
  Int ->
  (Point -> Double) ->
  (Point, Point) ->
  Maybe Int ->
  g ->
  m (V.Vector (Point, Double))
crsInit nDim f ((Point lb), (Point ub)) mPopulation g = do
  let nPop' =
        case mPopulation of
          Nothing -> 10 * (nDim + 1)
          Just pop -> pop
      nPop = max nPop' (nDim + 1)
  ps <-
    V.replicateM nPop $
      VU.forM (VU.fromList [0 .. nDim - 1]) $ \j ->
        uniformRM ((lb VU.! j), (ub VU.! j)) g
  let s0 = fmap (\p -> (Point p, f (Point p))) ps
  pure s0

crsMinimise :: MinimisationProblem -> MinimisationResult
crsMinimise MinimisationProblem {..} = runST do
  g <- System.Random.MWC.create
  s0 <- crsInit nDim objective (lowerBound, upperBound) initialPopulation g
  iter 1 g s0
  where
    iter i g s0 = do
      let (ib, iw) = bestWorstIdx s0
          sb = s0 V.! ib
          sw = s0 V.! iw
          (st, epsReached) = checkStoppingCriteria sb sw fEps
      if not st
        then
          if i <= maxIter
            then do
              s1 <- crsTrial nDim objective (lowerBound, upperBound) g s0 (ib, iw)
              iter (i + 1) g s1
            else pure (MaxTrialsReached sb epsReached)
        else pure (Okay sb i)
