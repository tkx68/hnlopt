{-# LANGUAGE Strict #-}

module Math.NLOpt.Point (
  -- * Points
  Point (..), 
  Point', 
  SearchSample, 
  -- * General helper functions
  checkStoppingCriteria, 
  bounded, idxs, addPoints, addAllPoints, subtractPoints, scalePoint, bestWorstIdx) where

import Data.Vector as V hiding ((++))
import Data.Vector.Unboxed as VU hiding ((++))

newtype Point = Point {coords :: VU.Vector Double}
  deriving (Show, Eq, Ord)

type Point' = (Point, Double)

type SearchSample = V.Vector Point'

idxs :: V.Vector a -> V.Vector Int -> V.Vector a
{-# INLINE idxs #-}
idxs v is = V.map (\i -> v V.! i) is

addPoints :: Point -> Point -> Point
{-# INLINE addPoints #-}
addPoints (Point v) (Point w) = Point $ VU.zipWith (+) v w

addAllPoints :: V.Vector Point -> Point
{-# INLINE addAllPoints #-}
addAllPoints v = Point . VU.convert $ V.foldr1 (VU.zipWith (+)) (V.map coords v)

subtractPoints :: Point -> Point -> Point
{-# INLINE subtractPoints #-}
subtractPoints (Point v) (Point w) = Point $ VU.zipWith (-) v w

scalePoint :: Point -> Double -> Point
{-# INLINE scalePoint #-}
scalePoint (Point v) x = Point $ VU.map (* x) v

bounded :: Point -> (Point, Point) -> Bool
{-# INLINE bounded #-}
bounded (Point x) ((Point lb), (Point ub)) =
  VU.and (VU.zipWith3 (\x' l u -> x' >= l && x' <= u) x lb ub)

bestWorstIdx :: V.Vector (a, Double) -> (Int, Int)
bestWorstIdx s =
  let iBest = V.minIndexBy (\a b -> compare (snd a) (snd b)) s :: Int
      iWorst = V.maxIndexBy (\a b -> compare (snd a) (snd b)) s :: Int
   in (iBest, iWorst)

checkStoppingCriteria :: Point' -> Point' -> Double -> (Bool, Double)
checkStoppingCriteria (_, fb) (_, fw) feps =
  let e = abs (fw - fb)
   in (e < feps, e)
