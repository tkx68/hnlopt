{-# LANGUAGE Strict #-}

module Math.NLOpt.Problem (
  -- * Definition of an optimisation problem
  MinimisationProblem (..), 
  MinimisationResult (..)
) where

import Data.Text (Text)
import Data.Text qualified as T
import Math.NLOpt.Point

data MinimisationResult
  = Okay {minimum :: Point', nIter :: Int}
  | MaxTrialsReached {minimum :: Point', fEpsAchieved :: Double}
  deriving (Show)

data MinimisationProblem = MinimisationProblem
  { -- | Name of the Problem (non-essential)
    name :: Text,
    -- | Dimension of the problem
    nDim :: Int,
    -- | Objective function to minimise
    objective :: Point -> Double,
    -- | lower bounds
    lowerBound :: Point,
    -- | upper bounds
    upperBound :: Point,
    -- | Initial population (if 'Nothing' use default value '10 * (nDim + 1)')
    initialPopulation :: Maybe Int,
    -- | Maximum number of iterations
    maxIter :: Int,
    -- | Desired objective function value accuracy
    fEps :: Double
  }

instance Show MinimisationProblem where
  show MinimisationProblem {..} =
    "Problem: "
      ++ T.unpack name
      ++ ", nDim: "
      ++ show nDim
      ++ ", lower bounds: "
      ++ show (coords lowerBound)
      ++ ", upper bounds: "
      ++ show (coords upperBound)
      ++ ", initial population: "
      ++ show initialPopulation
      ++ ", maximum iterations: "
      ++ show maxIter
      ++ ", objective epsilon: "
      ++ show fEps
