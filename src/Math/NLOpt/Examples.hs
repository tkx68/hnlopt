module Math.NLOpt.Examples where

import Data.Vector.Unboxed as VU hiding ((++))
import Math.NLOpt
import Math.NLOpt.Point

-- See https://en.wikipedia.org/wiki/Test_functions_for_optimization

ackley :: Point -> Double
ackley (Point v) =
  let x = v VU.! 0
      y = v VU.! 0
      e = exp (1)
   in -20
        * exp
          ( -0.2 * sqrt (0.5 * (x * x + y * y))
              - exp (0.5 * cos (2 * pi * x) + cos (2 * pi * y))
          )
        + e
        + 20

sphere :: Point -> Double
sphere = VU.sum . VU.map (** 2) . coords

ack :: Int -> Double -> MinimisationProblem
ack maxIter fEps = MinimisationProblem "Ackley" 2 ackley (Point $ VU.fromList [-5, -5]) (Point $ VU.fromList [5, 5]) Nothing maxIter fEps

sph :: Int -> Int -> Double -> MinimisationProblem
sph maxIter nDim fEps =
  let b = 1 :: Double
   in MinimisationProblem "Sphere" nDim sphere (Point $ VU.replicate nDim (- b)) (Point $ VU.replicate nDim b) Nothing maxIter fEps
